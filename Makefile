ifndef CI_REGISTRY_IMAGE
	CI_REGISTRY_IMAGE := "registry.gitlab.com/o.lelenkov/docker-dlang-cv-dev"
endif

all: build

build:
	@docker build --rm --tag=${CI_REGISTRY_IMAGE}:latest .

release: build
	@docker build --rm --tag=${CI_REGISTRY_IMAGE}:$(shell cat VERSION) .

publish: release
	@docker push ${CI_REGISTRY_IMAGE}

