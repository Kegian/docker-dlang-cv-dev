FROM registry.gitlab.com/o.lelenkov/docker-dlang-dev:2.82.1
MAINTAINER Oleg Lelenkov "o.lelenkov@gmail.com"

ENV OPENCV_VERSION=3.4.1
ENV DLIB_VERSION=19.7

# install OpenCV
COPY ./setup/opencv.sh /tmp/opencv.sh
RUN chmod +x /tmp/opencv.sh && sh /tmp/opencv.sh && rm -rf /tmp/opencv.sh

# install DLib
COPY ./setup/dlib.sh /tmp/dlib.sh
RUN chmod +x /tmp/dlib.sh && sh /tmp/dlib.sh && rm -rf /tmp/dlib.sh

RUN  apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/*
