#!/bin/bash
CPUC=$(awk '/^processor/{n+=1}END{print n}' /proc/cpuinfo)

install_packages wget libblas-dev libopenblas-dev liblapack-dev && ldconfig

# install Dlib
cd /tmp
wget http://dlib.net/files/dlib-${DLIB_VERSION}.tar.bz2 -O dlib.tar.bz2
tar -xjf dlib.tar.bz2
cd /tmp/dlib-${DLIB_VERSION}
mkdir build
cd build

cmake \
    -DUSE_SSE4_INSTRUCTIONS:BOOL=ON \
    -DUSE_AVX_INSTRUCTIONS:BOOL=ON \
    -DDLIB_NO_GUI_SUPPORT=ON \
    -DDLIB_DISABLE_ASSERTS=ON \
    -DDLIB_USE_CUDA=OFF \
    -DDLIB_USE_BLAS=ON \
    -DDLIB_USE_LAPACK=ON \
    ..

make -j${CPUC} && make install && ldconfig

rm -rf /tmp/dlib-${DLIB_VERSION}
rm -f /tmp/dlib.tar.bz2
