#!/bin/bash

CPUC=$(awk '/^processor/{n+=1}END{print n}' /proc/cpuinfo)

install_packages \
    pkg-config libtbb-dev libavcodec-dev libswscale-dev libavutil-dev libavformat-dev \
    libblas-dev libopenblas-dev liblapack-dev liblapacke-dev checkinstall \
    python python-pip python-numpy libpython2.7-dev && \
ldconfig

cd /tmp/
git clone https://github.com/opencv/opencv_contrib.git -b ${OPENCV_VERSION}
git clone https://github.com/opencv/opencv.git -b ${OPENCV_VERSION}

cd /tmp/opencv
mkdir build
cd build

cmake \
    CMAKE_BUILD_TYPE=RELEASE \
     -DCMAKE_BUILD_TYPE=Release \
     -DCMAKE_INSTALL_PREFIX=/usr/local \
     -DOPENCV_EXTRA_MODULES_PATH=/tmp/opencv_contrib/modules/ \
     -DOpenCV_STATIC=OFF \
     -DBUILD_PERF_TESTS=OFF \
     -DBUILD_TESTS=OFF \
     -DINSTALL_TESTS=OFF \
     -DBUILD_SHARED_LIBS=ON \
     -DBUILD_ANDROID_EXAMPLES=OFF \
     -DBUILD_DOCS=OFF \
     -DBUILD_EXAMPLES=OFF \
     -DBUILD_JASPER=ON \
     -DBUILD_FAT_JAVA_LIB=OFF \
     -DWITH_TBB=ON \
     -DBUILD_JPEG=ON \
     -DBUILD_PNG=ON \
     -DBUILD_TIFF=ON \
     -DBUILD_ZLIB=ON \
     -DWITH_GTK=OFF \
     -DBUILD_opencv_python2=ON \
     -DWITH_CUDA=OFF \
     -DWITH_OPENCL=ON \
     ..

make -j${CPUC} && make install && ldconfig

rm -rf /tmp/opencv
rm -rf /tmp/opencv_contrib
